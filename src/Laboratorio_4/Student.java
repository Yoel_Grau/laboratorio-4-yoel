package Laboratorio_4;

import java.util.Comparator;
import java.util.Objects;


public class Student implements Comparable<Student> {
    private String name;
    private int grade;

    public Student(String name, int grade) {
        this.name = name;
        this.grade = grade;
    }


    public static Comparator<Student> byGrade() {
        return new Comparator<Student>() {
            @Override
            public int compare(Student student1, Student student2) {
                return Integer.compare(student1.getGrade(), student2.getGrade());
            }
        };
    }


    @Override
    public int compareTo(Student other) {
        int nameComparison = this.name.compareTo(other.name);
        if (nameComparison != 0) {
            return nameComparison;
        }
        return Integer.compare(this.grade, other.grade);
    }


    @Override
    public int hashCode() {
        return Objects.hash(name, grade);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (grade != student.grade) return false;

        if (name == null) {
            return student.name == null;
        } else {
            return name.equals(student.name);
        }
    }


    public String getName() {
        return name;
    }

    public int getGrade() {
        return grade;
    }

    public String toString() {
        return "{" + name + "," + grade + "}";
    }

}


