package Laboratorio_4;

import java.util.AbstractList;
import java.util.Comparator;
import java.util.Iterator;

public class UniArrayList<T> extends AbstractList<T> implements Sortable<T>, Unique<T> {
    private Object[] elements;
    private int size = 0;


    public UniArrayList(T[] initial) {
        elements = new Object[initial.length];
        for (int i = 0; i < initial.length; i++) {
            elements[i] = initial[i];
        }
        size = initial.length;
    }


    public UniArrayList() {
        elements = new Object[10];
    }


    @Override
    public T get(int index) {
        if (index < 0 || index >= size) {
            return null;
        }
        return (T) elements[index];
    }


    @Override
    public int size() {
        return size;
    }


    @Override
    public T remove(int index) {
        if (index < 0 || index >= size) {
            return null;
        }

        T oldValue = (T) elements[index];

        int numMoved = size - index - 1;
        if (numMoved > 0) {
            for (int i = index; i < size - 1; i++) {
                elements[i] = elements[i + 1];
            }
        }

        elements[--size] = null;

        return oldValue;
    }


    private void assureCapacity(int minCapacity) {
        if (minCapacity <= elements.length) {
            return;
        }

        int newCapacity;
        if (elements.length * 2 > elements.length) {
            newCapacity = elements.length * 2;
        } else {
            newCapacity = Integer.MAX_VALUE;
        }

        if (newCapacity < minCapacity) {
            newCapacity = minCapacity;
        }

        Object[] newElements = new Object[newCapacity];

        for (int i = 0; i < size; i++) {
            newElements[i] = elements[i];
        }

        elements = newElements;
    }


    @Override
    public boolean add(T element) {
        assureCapacity(size + 1);
        elements[size++] = element;
        return true;
    }


    @Override
    public void add(int index, T element) {
        if (index < 0 || index > size) {
            index = size;
        }

        assureCapacity(size + 1);

        for (int i = size; i > index; i--) {
            elements[i] = elements[i - 1];
        }

        elements[index] = element;
        size++;
    }


    @Override
    public boolean remove(Object obj) {
        for (int index = 0; index < size; index++) {
            if (obj.equals(elements[index])) {
                remove(index);
                return true;
            }
        }
        return false;
    }


    @Override
    public void sort() {
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - i - 1; j++) {

                Student a = (Student) elements[j];

                Student b = (Student) elements[j + 1];

                if (a.compareTo(b) > 0) {
                    Object temp = elements[j];
                    elements[j] = elements[j + 1];
                    elements[j + 1] = temp;
                }
            }
        }
    }


    @Override
    public void sortBy(Comparator<Student> comparator) {
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - i - 1; j++) {

                Student a = (Student) elements[j];

                Student b = (Student) elements[j + 1];

                if (comparator.compare(a, b) > 0) {
                    Object temp = elements[j];
                    elements[j] = elements[j + 1];
                    elements[j + 1] = temp;
                }
            }
        }
    }


    @Override
    public void unique() {
        int uniqueCount = 0;
        for (int i = 0; i < size; i++) {
            boolean isDuplicate = false;
            for (int j = 0; j < uniqueCount; j++) {
                if (elements[i].equals(elements[j])) {
                    isDuplicate = true;
                    break;
                }
            }
            if (!isDuplicate) {
                elements[uniqueCount++] = elements[i];
            }
        }
        for (int i = uniqueCount; i < size; i++) {
            elements[i] = null;
        }
        size = uniqueCount;
    }


    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < size;
            }

            @Override
            public T next() {
                if (!hasNext()) {
                    return null;
                }
                return (T) elements[currentIndex++];
            }
        };
    }

}



