package Laboratorio_4;

import java.util.Comparator;

public interface Sortable<T> {
    void sort();
    void sortBy(Comparator<Student> comparator);
}




